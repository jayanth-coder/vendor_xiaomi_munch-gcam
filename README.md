# Setting up Google Camera
To build Google Camera you have to build the package in your device tree (Example, in device.mk).
```bash
# Inherit Google Camera
$(call inherit-product-if-exists, vendor/xiaomi/munch-gcam/config.mk) 
```

# Credits
* [**BSG Team**](https://t.me/Channel_MGC_BSG)
